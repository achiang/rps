import React, { useState } from 'react';

export default function PlayerTurn(props) {
  const initialMove = 'Rock';
  const [move, setMove] = useState(initialMove);

  return (
    <div className="turn">
      <h2>{props.player}</h2>
      <div>
        <label>
          Select move
          <select
            onChange={e => {
              setMove(e.target.value);
            }}
            name="move"
            value={move}
          >
            <option value="Rock">Rock</option>
            <option value="Paper">Paper</option>
            <option value="Scissors">Scissors</option>
          </select>
        </label>
      </div>
      <div>
        <button
          type="button"
          onClick={e => {
            props.setTurn(move);
            setMove(initialMove);
          }}
        >
          Ok
        </button>
      </div>
    </div>
  );
}
