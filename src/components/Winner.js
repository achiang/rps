import React from 'react';

export default function Winner(props) {
  return (
    <div className="winner">
      <h1>We have a winner!</h1>
      <p>Player {props.winner} is the new emperor!</p>
      <button type="button" onClick={props.reset}>
        Play again
      </button>
    </div>
  );
}
