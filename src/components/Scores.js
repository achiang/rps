import React from 'react';

export default function Scores(props) {
  return (
    <div className="scores">
      <h3>Scores</h3>

      <table>
        <tbody>
          <tr>
            <th>Round</th>
            <th>Winner</th>
          </tr>
          {props.rounds.map((win, index) => (
            <React.Fragment key={index}>
              <tr>
                <td>{index + 1}</td>
                <td>{win}</td>
              </tr>
            </React.Fragment>
          ))}
        </tbody>
      </table>
    </div>
  );
}
