import React from 'react';

import styles from './TwoCols.module.css';

export default function TwoCols(props) {
  return <div className={styles.twoCols}>{props.children}</div>;
}
