import React, { useState } from 'react';

import PlayerTurn from './PlayerTurn';
import TwoCols from './TwoCols';
import Panel from './Panel';

export default function Round(props) {
  const [player1Move, setPlayer1Move] = useState('');
  const [player2Move, setPlayer2Move] = useState('');

  const rules = {
    Paper: 'Rock',
    Rock: 'Scissors',
    Scissors: 'Paper',
  };

  if (player1Move && player2Move) {
    setPlayer1Move('');
    setPlayer2Move('');
    if (rules[player1Move] === player2Move) {
      props.winRound(props.players.player1);
    } else if (rules[player2Move] === player1Move) {
      props.winRound(props.players.player2);
    } else {
      props.winRound('Tie');
    }
  }

  const { player1, player2 } = props.players;
  const currentPlayer = !player1Move ? player1 : player2;
  const setPlayerMove = !player1Move ? setPlayer1Move : setPlayer2Move;

  return (
    <div className="round">
      <Panel>
        <h1>Round {props.round}</h1>
      </Panel>
      <TwoCols>
        <Panel>
          <PlayerTurn player={currentPlayer} setTurn={setPlayerMove} />
        </Panel>
        <Panel>{props.scores}</Panel>
      </TwoCols>
    </div>
  );
}
