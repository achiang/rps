import React, { useState } from 'react';

import Panel from './Panel';

export default function Start(props) {
  const [player1, setPlayer1] = useState('');
  const [player2, setPlayer2] = useState('');

  return (
    <Panel>
      <h1>Enter players' names</h1>
      <div>
        <label>
          Player 1
          <input
            type="text"
            value={player1}
            name="player1"
            onChange={e => {
              setPlayer1(e.target.value);
            }}
          />
        </label>
      </div>
      <div>
        <label>
          Player 2
          <input
            type="text"
            value={player2}
            name="player2"
            onChange={e => {
              setPlayer2(e.target.value);
            }}
          />
        </label>
      </div>
      <div>
        <button
          type="button"
          onClick={() => {
            props.setPlayers({ player1, player2 });
          }}
        >
          Start
        </button>
      </div>
    </Panel>
  );
}
