import React, { useState } from 'react';

import Start from '../Start';
import Game from '../Game';
import Winner from '../Winner';
import styles from './App.module.css';

function App() {
  const initialPlayers = { player1: '', player2: '' };
  const [players, setPlayers] = useState(initialPlayers);

  const [winner, setWinner] = useState('');
  const isStart = players.player1 === '' || players.player2 === '';
  const havePlayers = players.player1 !== '' && players.player2 !== '';
  const inGame = havePlayers && winner === '';
  const isGameFinished = winner !== '';

  function reset() {
    setPlayers(initialPlayers);
    setWinner('');
  }

  return (
    <main role="main" className={styles.main}>
      {isStart ? (
        <Start setPlayers={setPlayers} />
      ) : inGame ? (
        <Game players={players} setWinner={setWinner} />
      ) : isGameFinished ? (
        <Winner winner={winner} reset={reset} />
      ) : (
        'Oops!'
      )}
    </main>
  );
}

export default App;
