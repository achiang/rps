import React, { useState } from 'react';

import Round from './Round';
import Scores from './Scores';

export default function Game(props) {
  const [currentRound, setCurrentRound] = useState(1);
  const [rounds, setRounds] = useState([]);

  function winRound(winner) {
    setCurrentRound(currentRound + 1);
    const newRounds = [...rounds, winner];
    setRounds(newRounds);

    const [player1Wins, player2Wins] = newRounds.reduce(
      ([accWins1, accWins2], winner) => {
        return [
          winner === props.players.player1 ? accWins1 + 1 : accWins1,
          winner === props.players.player2 ? accWins2 + 1 : accWins2,
        ];
      },
      [0, 0]
    );

    if (player1Wins === 3) {
      props.setWinner(props.players.player1);
    }

    if (player2Wins === 3) {
      props.setWinner(props.players.player2);
    }
  }

  return (
    <div>
      <Round
        round={currentRound}
        players={props.players}
        winRound={winRound}
        scores={<Scores rounds={rounds} />}
      />
    </div>
  );
}
