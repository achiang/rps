## Rock Paper Scissors

Install the required modules:

`yarn`

Run the app in development mode.

`yarn start`

And visit http://localhost:3000.

To run the functional test:

`yarn cypress run`
