/// <reference types="Cypress" />

describe('Basic test', () => {
  it('Just three wins in a row', () => {
    // go to start url
    cy.visit('http://localhost:3000');
    cy.contains("Enter players' names");

    // system asks for the name of each player
    cy.contains('Player 1')
      .get('input[name=player1]')
      .type('Paul')
      .should('have.value', 'Paul');

    cy.contains('Player 2')
      .get('input[name=player2]')
      .type('John')
      .should('have.value', 'John');

    cy.contains('Start').click();

    // the game starts with round 1
    cy.contains('Round 1');
    // the system asks player 1 for a move
    cy.contains('Paul');
    cy.contains('Select move')
      .get('select[name=move]')
      .select('Paper');
    cy.contains('Ok').click();

    // then asks player 2 for a move
    cy.contains('John');
    cy.contains('Select move')
      .get('select[name=move]')
      .select('Scissors');
    cy.contains('Ok').click();

    // winner should be shown on the side
    cy.get('table').contains('John');

    // repeat until a player wins three times
    cy.contains('Round 2');

    cy.contains('Paul');
    cy.contains('Select move')
      .get('select[name=move]')
      .select('Paper');
    cy.contains('Ok').click();

    cy.contains('John');
    cy.contains('Select move')
      .get('select[name=move]')
      .select('Scissors');
    cy.contains('Ok').click();

    cy.contains('Round 3');

    cy.contains('Paul');
    cy.contains('Select move')
      .get('select[name=move]')
      .select('Paper');
    cy.contains('Ok').click();

    // then asks player 2 for a move
    cy.contains('John');
    cy.contains('Select move')
      .get('select[name=move]')
      .select('Scissors');
    cy.contains('Ok').click();

    // show finish screen with the winner
    cy.contains('We have a winner');
    cy.contains('John');

    // click button and go to start screen
    cy.contains('Play again').click();
  });
});
